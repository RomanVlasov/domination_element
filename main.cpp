#include <vector>
#include <tuple>
#include <algorithm>
#include <iostream>
#include <list>
#include <string>
#include <sstream>

enum {Count, Value};

template<typename T, typename TIter>
size_t check_condition(TIter begin, TIter end, T&& value, size_t current_count, size_t need_count)
{
    const auto count = std::count(begin, end, value) + current_count;
    if (count >= need_count)
    {
        return count;
    }
    return 0;
}

template<typename TIter>
auto domination_element(TIter begin, TIter end) -> std::tuple<size_t, typename std::remove_reference<decltype(*begin)>::type>
{
    using T = typename std::remove_reference<decltype(*begin)>::type;

    const auto num_elements = std::distance(begin, end);
    if( num_elements == 0 )
    {
        return std::make_tuple(0, T());
    }

    if( num_elements == 1 )
    {
        return std::make_tuple(1, *begin);
    }

    const auto partion_number = num_elements / 2;
    const auto need_number = num_elements / 2 + 1;
    auto partion_element = begin;
    std::advance(partion_element, partion_number);
    const auto first_partion_result = domination_element(begin, partion_element);
    const auto second_partion_result = domination_element(partion_element, end);
    if (std::get<Count>(first_partion_result) > 0)
    {
        const auto count = check_condition(partion_element,
                                     end,
                                     std::get<Value>(first_partion_result),
                                     std::get<Count>(first_partion_result),
                                     need_number);
        if (count > 0)
        {
            return std::make_tuple(count, std::get<Value>( first_partion_result ));
        }
    }

    if (std::get<Count>(second_partion_result) > 0)
    {
        const auto count = check_condition(begin,
                                     partion_element,
                                     std::get<Value>(second_partion_result),
                                     std::get<Count>(second_partion_result),
                                     need_number);
        if (count > 0)
        {
            return std::make_tuple(count, std::get<Value>(second_partion_result));
        }
    }

    return std::make_tuple(0, T());
}


int main()
{
    std::vector<int> arr = {11, 11, 2, 56, 1, 89, 90, 356, 11, 11, 11, 3, 11, 11, 11};
    std::list<std::string> str_arr;

    std::transform(std::begin(arr), std::end(arr), std::back_inserter(str_arr),
                   [](int value) -> std::string
                   {
                       std::stringstream ss;
                       ss << value;
                       return ss.str();
                    } );

    auto res = domination_element(arr.begin(), arr.end());
    std::cout << std::get<Value>( res ) << std::endl;
    std::cout << std::get<Count>( res ) << std::endl;

    auto str_res = domination_element(str_arr.rbegin(), str_arr.rend());
    std::cout << std::get<Value>(str_res) << std::endl;
    std::cout << std::get<Count>(str_res) << std::endl;

    /*
    std::vector<std::vector<int>> values = {{23, 13, 25}, {23, 13, 250}, {13, 13, 25}};
    std::vector<std::future<decltype(values)::value_type>> futures;

    for( auto vec : values )
    {
        auto f = std::async( [&vec]{ std::sort( vec.begin(), vec.end() ); } );
    }*/

	system("PAUSE");
    return 0;
}
